package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	flagUpdateDisabled     = "update-disabled"
	flagAdvisoryDBURL      = "advisory-db-url"
	flagAdvisoryDBRefName  = "advisory-db-ref-name"
	flagAdvisoryDBUserHome = "advisory-db-user-home"

	// databaseRelPath is the path to the vulnerability database
	// relative to the user HOME
	databaseRelPath = ".local/share/ruby-advisory-db"

	remoteName = "origin"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.BoolFlag{
			Name:    flagUpdateDisabled,
			Usage:   "Do not update the advisory database before scanning",
			EnvVars: []string{"BUNDLER_AUDIT_UPDATE_DISABLED"},
		},
		&cli.StringFlag{
			Name:    flagAdvisoryDBURL,
			Usage:   "The url of the bundler-audit advisory db",
			EnvVars: []string{"BUNDLER_AUDIT_ADVISORY_DB_URL"},
			Value:   "https://github.com/rubysec/ruby-advisory-db.git",
		},
		&cli.StringFlag{
			Name:    flagAdvisoryDBRefName,
			Usage:   "The name of the git ref to use when checking out the advisory db",
			EnvVars: []string{"BUNDLER_AUDIT_ADVISORY_DB_REF_NAME"},
			Value:   "master",
		},
		&cli.StringFlag{
			Name:    flagAdvisoryDBUserHome,
			Usage:   "User HOME (base directory for the vulnerability database)",
			EnvVars: []string{"BUNDLER_AUDIT_ADVISORY_DB_USER_HOME"},
			Value:   os.Getenv("HOME"),
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	if !c.Bool(flagUpdateDisabled) {
		if err := updateCustomAdvisoryDB(c); err != nil {
			return nil, err
		}
	}

	cmd := exec.Command("bundle", "audit", "check")
	cmd.Dir = path

	// Force user HOME to control the path to the vulnerability database (git clone of ruby-advisory-db)
	// see https://github.com/rubysec/bundler-audit/blob/v0.7.0/lib/bundler/audit/database.rb#L41
	// TODO: use --database CLI option after updating to bundler-audit v0.8.0
	cmd.Env = append(os.Environ(), "HOME="+c.String(flagAdvisoryDBUserHome))

	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		// bundler-audit exits with code 1 when they are vulnerabilities,
		// so ignore the exit code if the output contains "Vulnerabilities found!".
		if !strings.Contains(string(output), "Vulnerabilities found!") {
			if _, err := os.Stdout.Write(output); err != nil {
				log.Error(err)
			}
			return nil, err
		}
	}

	return ioutil.NopCloser(bytes.NewReader(output)), nil
}

func updateCustomAdvisoryDB(c *cli.Context) error {
	log.Info("Updating vulnerability database")

	git := func(args ...string) *exec.Cmd {
		// path to the vulnerability database is relative to user HOME
		// see https://github.com/rubysec/bundler-audit/blob/v0.7.0/lib/bundler/audit/database.rb#L41
		// TODO: simplify this when updating to bundler-audit v0.8.0
		args = append([]string{"-C", filepath.Join(c.String(flagAdvisoryDBUserHome), databaseRelPath)}, args...)
		cmd := exec.Command("git", args...)
		return cmd
	}

	argsList := [][]string{
		{"remote", "set-url", remoteName, c.String(flagAdvisoryDBURL)},
		{"fetch", "--force", "--tags", remoteName, c.String(flagAdvisoryDBRefName)},
		{"checkout", c.String(flagAdvisoryDBRefName)},
	}

	for _, args := range argsList {
		cmd := git(args...)
		output, err := cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), output)
		if err != nil {
			return err
		}
	}

	// ref is the commit ref to which HEAD will be reset
	ref := c.String(flagAdvisoryDBRefName)

	// check whether the current HEAD is a branch
	// if branch then ensure HEAD gets reset to the remote copy
	// by adding remote name to the git ref
	cmd := git("symbolic-ref", "-q", "HEAD")
	log.Debugf("%s", cmd.String())
	if err := cmd.Run(); err == nil { // branch
		ref = fmt.Sprintf("%s/%s", remoteName, ref)
	}

	// reset HEAD to ref at remote
	cmd = git("reset", "--hard", ref)
	out, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), out)
	if err != nil {
		return err
	}

	// log the repo's commit id at HEAD
	cmd = git("rev-parse", "HEAD")
	out, err = cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), out)
	if err != nil {
		return err
	}
	log.Infof("Using commit %s of vulnerability database\n", out)

	return nil
}
