module gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2

require (
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.1.2
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.24.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v2 v2.1.0
	golang.org/x/sys v0.0.0-20210908233432-aa78b53d3365 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)

go 1.13
