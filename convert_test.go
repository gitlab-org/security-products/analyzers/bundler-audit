package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

func TestConvert(t *testing.T) {
	var scanner = metadata.IssueScanner

	testcases := []struct {
		name            string
		input           string
		vulnerabilities []report.Vulnerability
	}{
		{
			name: "3 vulnerabilities, no warnings",
			input: `

Advisory: CVE-2018-1000201
Version: 1.9.18
Name: ffi
Criticality: High
URL: https://github.com/ffi/ffi/releases/tag/1.9.24
Title: ruby-ffi DDL loading issue on Windows OS
Solution: upgrade to >= 1.9.24

Name: loofah
Version: 2.1.1
Advisory: CVE-2018-16468
Criticality: Unknown
URL: https://github.com/flavorjones/loofah/issues/154
Title: Loofah XSS Vulnerability
Solution: upgrade to >= 2.2.3


Name: mail
Version: 2.4.4
Advisory: 131677
Criticality: Unknown
URL: https://hackerone.com/reports/137631
Title: SMTP command injection
Solution: upgrade to >= 2.5.5

Vulnerabilities found! `,
			vulnerabilities: []report.Vulnerability{
				{
					Category:   report.CategoryDependencyScanning,
					Message:    "ruby-ffi DDL loading issue on Windows OS",
					CompareKey: "app/Gemfile.lock:ffi:cve:CVE-2018-1000201",
					Severity:   report.SeverityLevelHigh,
					Scanner:    scanner,
					Location: report.Location{
						File: "app/Gemfile.lock",
						Dependency: &report.Dependency{
							Package: report.Package{Name: "ffi"},
							Version: "1.9.18",
						},
					},
					Identifiers: []report.Identifier{
						{
							Type:  report.IdentifierTypeCVE,
							Name:  "CVE-2018-1000201",
							Value: "CVE-2018-1000201",
							URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1000201",
						},
					},
					Links: []report.Link{
						{
							URL: "https://github.com/ffi/ffi/releases/tag/1.9.24",
						},
					},
					Solution: "upgrade to >= 1.9.24",
				},
				{
					Category:   report.CategoryDependencyScanning,
					Message:    "Loofah XSS Vulnerability",
					CompareKey: "app/Gemfile.lock:loofah:cve:CVE-2018-16468",
					Severity:   report.SeverityLevelUnknown,
					Scanner:    scanner,
					Location: report.Location{
						File: "app/Gemfile.lock",
						Dependency: &report.Dependency{
							Package: report.Package{Name: "loofah"},
							Version: "2.1.1",
						},
					},
					Identifiers: []report.Identifier{
						{
							Type:  report.IdentifierTypeCVE,
							Name:  "CVE-2018-16468",
							Value: "CVE-2018-16468",
							URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16468",
						},
					},
					Links: []report.Link{
						{
							URL: "https://github.com/flavorjones/loofah/issues/154",
						},
					},
					Solution: "upgrade to >= 2.2.3",
				},
				{
					Category:   report.CategoryDependencyScanning,
					Message:    "SMTP command injection",
					CompareKey: "app/Gemfile.lock:mail:osvdb:OSVDB-131677",
					Severity:   report.SeverityLevelUnknown,
					Scanner:    scanner,
					Location: report.Location{
						File: "app/Gemfile.lock",
						Dependency: &report.Dependency{
							Package: report.Package{Name: "mail"},
							Version: "2.4.4",
						},
					},
					Identifiers: []report.Identifier{
						{
							Type:  report.IdentifierTypeOSVDB,
							Name:  "OSVDB-131677",
							Value: "OSVDB-131677",
							URL:   "https://cve.mitre.org/data/refs/refmap/source-OSVDB.html",
						},
					},
					Links: []report.Link{
						{
							URL: "https://hackerone.com/reports/137631",
						},
					},
					Solution: "upgrade to >= 2.5.5",
				},
			},
		},
		{
			name: "1 vulnerability, 1 warning",
			input: `Warning: the running version of Bundler (2.1.4) is older than the version that created the lockfile (2.2.32).

Advisory: CVE-2018-1000201
Version: 1.9.18
Name: ffi
Criticality: High
URL: https://github.com/ffi/ffi/releases/tag/1.9.24
Title: ruby-ffi DDL loading issue on Windows OS
Solution: upgrade to >= 1.9.24

Vulnerabilities found! `,
			vulnerabilities: []report.Vulnerability{
				{
					Category:   report.CategoryDependencyScanning,
					Message:    "ruby-ffi DDL loading issue on Windows OS",
					CompareKey: "app/Gemfile.lock:ffi:cve:CVE-2018-1000201",
					Severity:   report.SeverityLevelHigh,
					Scanner:    scanner,
					Location: report.Location{
						File: "app/Gemfile.lock",
						Dependency: &report.Dependency{
							Package: report.Package{Name: "ffi"},
							Version: "1.9.18",
						},
					},
					Identifiers: []report.Identifier{
						{
							Type:  report.IdentifierTypeCVE,
							Name:  "CVE-2018-1000201",
							Value: "CVE-2018-1000201",
							URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1000201",
						},
					},
					Links: []report.Link{
						{
							URL: "https://github.com/ffi/ffi/releases/tag/1.9.24",
						},
					},
					Solution: "upgrade to >= 1.9.24",
				},
			},
		},
		{
			name:            "no vulnerabilities, no warnings",
			input:           "No vulnerabilities found",
			vulnerabilities: []report.Vulnerability{},
		},
		{
			name: "no vulnerabilities, 1 warning",
			input: `Warning: the running version of Bundler (2.1.4) is older than the version that created the lockfile (2.2.32).
No vulnerabilities found`,
			vulnerabilities: []report.Vulnerability{},
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			r := strings.NewReader(tc.input)
			want := &report.Report{
				Version:         report.CurrentVersion(),
				Vulnerabilities: tc.vulnerabilities,
				Remediations:    []report.Remediation{},
				DependencyFiles: []report.DependencyFile{},
			}
			got, err := convert(r, "app")
			require.NoError(t, err)
			require.Equal(t, want, got)
		})
	}
}
